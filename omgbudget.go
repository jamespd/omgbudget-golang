package main

import (
	"encoding/csv"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
	"time"

	"gopkg.in/yaml.v2"
)

// Example CSV
//Date,Amount,Other Party,Description,Reference,Particulars,Analysis Code
//04/01/2023,-50.30,"Gas Station","EFTPOS TRANSACTION","04-08:43-179","************","7461 16378"
//05/02/2024,-17.50,"Grocery Store","DEBIT",,"************","7461"

// Transaction
type Transaction struct {
	date time.Time
	amount float64
	other_party string
	description string
	reference string
	particulars string
	analysis_code string
	classification string
}

func (t Transaction) String() string {
    reportTimeFormat := "2006-01-02"
	return fmt.Sprintf("%v %v %v (%v)", t.date.Format(reportTimeFormat), t.other_party, t.amount, t.classification)
}

// Classification
type Classification struct {
	regex *regexp.Regexp
	classification string
}

// Report
type Report struct {
	start time.Time
	end time.Time
	transactions []Transaction
}

func getClassifications(fname string) []Classification {
	var classifications []Classification
    yamlFile, err := os.ReadFile(fname)
    if err != nil {
        log.Printf("yamlFile.Get err   #%v ", err)
    }
    err = yaml.Unmarshal(yamlFile, &classifications)
    if err != nil {
        log.Fatalf("Unmarshal: %v", err)
    }
    return classifications
}

func parseClassificationList(data [][]string) []Classification {
	var classifications []Classification
	for i, line := range data {
		if i > 0 { // omit header line
		    _ = line
			r, _ := regexp.Compile(line[1])
			rec := Classification{regex: r, classification: line[0]}
			classifications = append(classifications, rec)
		}
	}
	return classifications
}

func parseTransactionList(data [][]string) []Transaction {
	var transactionList []Transaction
	for i, line := range data {
		if i > 0 { // omit header line
			amount,_ := strconv.ParseFloat(line[1], 64)
			const layout = "2/1/2006"
			tm, _ := time.Parse(layout, line[0])
			rec := Transaction{date: tm, amount: amount, other_party: line[2], description: line[3], reference: line[4], particulars: line[5], analysis_code: line[6], classification: "unknown"}
			//for _, field := range line {
				//fmt.Printf("%+v\n",field)
			//}
			transactionList = append(transactionList, rec)
		}
	}
	return transactionList
}

func inTimeRange(start, end, check time.Time) bool {
	return check.After(start) && check.Before(end)
}

// Given a list of transactions, return the oldest and newest associated times
func transactionTimeSpan(transactions []Transaction) (time.Time, time.Time) {
	// TODO: Handle empty Transactions slice
	start := transactions[0].date
	end := transactions[0].date
	for _, transaction := range transactions {
		if transaction.date.Before(start) {
			start = transaction.date
		}
		if transaction.date.After(end) {
			end = transaction.date
		}
	}
	return start, end
}

func generateReport(report Report) {
    reportTimeFormat := "2006-01-02"
	fmt.Printf("Report Start: %v\n", report.start.Format(reportTimeFormat))
	fmt.Printf("Report End: %v\n", report.end.Format(reportTimeFormat))
	fmt.Printf("Transaction Count: %v\n", len(report.transactions))
	// Create a map and sum up all transactions that match each classification
	classification_totals := map[string]float64{}
	for _, transaction := range report.transactions {
		//fmt.Println(transaction)
		classification_totals[transaction.classification] += transaction.amount
	}
	for k, v := range classification_totals {
		fmt.Printf("%v %v\n", k, v)
	}
}

func main() {

	// Load Transactions
	f, err := os.Open("test-transactions.csv")
	if err != nil {
		log.Fatal(err)
	}
	csvReader := csv.NewReader(f)
	data, err := csvReader.ReadAll()
	if err != nil {
		log.Fatal(err)
	}
	f.Close()
	transactionList := parseTransactionList(data)

	// Load Classifications
	//f, err = os.Open("test-classifications.csv")
	//if err != nil {
	//	log.Fatal(err)
	//}
	//csvReader = csv.NewReader(f)
	//data, err = csvReader.ReadAll()
	//if err != nil {
	//	log.Fatal(err)
	//}
	//f.Close()
	//classifications := parseClassificationList(data)
    classifications := getClassifications("test-classifications.yml")

	// Get first and last transaction dates, to determine over which ranges, we should generate reports.
	start, end := transactionTimeSpan(transactionList)
	_,_ = start,end

	//// Load Classifications
	//var classifications []Classification
	//r, _ := regexp.Compile("Grocery Store")
	//classifications = append(classifications, Classification{regex: r , classification: "Groceries"})

	//r, _ = regexp.Compile("Gas")
	//classifications = append(classifications, Classification{regex: r , classification: "Fuel"})

	// Print each transaction
	for i := range transactionList {
		//fmt.Printf("%+v\n", transaction)
		for _, c := range classifications {
			if c.regex.MatchString(transactionList[i].other_party) {
				transactionList[i].classification = c.classification
			}
		}
		fmt.Println(transactionList[i])
	}

	// Create report
	report := Report{start: time.Date( 2022, 1, 1, 0, 0, 0, 0, time.UTC), end: time.Date( 2024, 2, 1, 0, 0, 0, 0, time.UTC) }
	for _, transaction := range transactionList {
		if inTimeRange(report.start, report.end, transaction.date) {
			//fmt.Printf("%v < %v < %v\n", report.start, transaction.date, report.end)
			report.transactions = append(report.transactions, transaction)
			//TODO: Why doesn't the classification get carried over?
		}
	}
	generateReport(report)
	//fmt.Println(report)
}
