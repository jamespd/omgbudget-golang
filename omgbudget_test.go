package main

import (
	"testing"
	"regexp"
	"time"
)

func TestTransaction(t *testing.T) {

// Test Transaction
	t.Run("Transaction Create", func(t *testing.T) {
		transaction := Transaction{date: time.Date( 2024, 1, 30, 20, 34, 58, 651387237, time.UTC), amount: 100.0,
                        other_party: "Grocery Store", description: "Description",
                        reference: "Reference", particulars: "particulars", analysis_code: "analysis_code"}
		want := 100.0
		got := transaction.amount
		if got != want {
			t.Errorf("got %f want %f", got, want)
		}
	})

// Test Classification
	t.Run("Transaction Classify", func(t *testing.T) {
		transaction := Transaction{date: time.Date( 2024, 1, 30, 20, 34, 58, 651387237, time.UTC), amount: 100.0,
                        other_party: "Grocery Store", description: "Description",
                        reference: "Reference", particulars: "particulars", analysis_code: "analysis_code"}
		r, _ := regexp.Compile("Grocery Store")
		classification := Classification{regex: r , classification: "Groceries"}
		want := true
		got := classification.regex.MatchString(transaction.other_party)
		if got != want {
			t.Errorf("Failed to classify %v as %v",transaction.other_party, classification.classification)
		}
	})

// TODO: CSV Transaction Load Test
}
