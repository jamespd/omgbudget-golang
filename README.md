# omgbudget-golang

A golang re-implementation of my Python/Django budgeting software: OMG Budget!

## Data Model

### MonthlyBudget

```
# Django Model
    name = models.CharField(max_length=200)
    # Because we may aggregate multiple classifications via 'classify_as', budgets should not be set against Classifications, but against what we deci
de to report on.
    amount = models.DecimalField(max_digits=10, decimal_places=2, default=0)

```

### Classification

```
# Django Model
    name = models.CharField(max_length=200)
    classify_as = models.CharField(max_length=200)
    always_report = models.CharField(max_length=200)
    # Because we may aggregate multiple classifications via 'classify_as', budgets should not be set against Classifications, but against what we decide to report on.
    monthly_budget = models.DecimalField(max_digits=10, decimal_places=2, default=0)

```

## Classification Regex

```
# Django Model
    regex = models.CharField(max_length=200)
    classification = models.ForeignKey(Classification, on_delete=models.CASCADE)
```

# Transaction

```
# Django Model
    description = models.CharField(max_length=200)
    other_party = models.CharField(max_length=200)
    reference = models.CharField(max_length=200)
    particulars = models.CharField(max_length=200)
    analysis_code = models.CharField(max_length=200)
    trans_date = models.DateField("Transaction Date")
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    classifications = models.ManyToManyField(Classification)
```

